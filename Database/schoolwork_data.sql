INSERT
INTO
  users(
  username,
  password,
  last_password_reset,
  authorities
  )
  VALUES ('admin', 
  '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi', 
  null, 
  'ADMIN'
  ), ('thaotran', 
  '$2a$10$DazdlOZa9/gPFnIz09Zm/OGT136Z/zHyfJd7yls6aOsz8YZH5ldDa', 
  null, 
  'EMPLOYEE'
  ), ('anhle', 
  '$2a$10$Nv6hHUjg.1dDHnwcvFP.6.uKY.Q3AyoLJL8ENSJp5jAF4hnCAL.We', 
  null, 
  'EMPLOYEE'
  ), ('khangtruong', 
  '$2a$10$1shQIEKNN/i83Iua7k01KOm3Jlni/Xa.tsBN9e8SjvkzF/.imL.Ze', 
  null, 
  'EMPLOYEE'
  ), ('tamle', 
  '$2a$10$HNONUEKU4pu5FsOntZbY/uG0OktWwdIabHTrDDsILH2GUpaGohg7y', 
  null, 
  'EMPLOYEE'
  ), ('ngavo', 
  '$2a$10$RlQT7Mh19oWq14883QmuOuhXdudq1MT33HmDyQJaC70QZWAGEuUh6', 
  null, 
  'EMPLOYEE'
  ), ('baole', 
  '$2a$10$uoZ1QFqilK.ffGouDURyuuWbPifPMeZ2MqrTfaiXNF304zcaZZzi6', 
  null, 
  'EMPLOYER'
  ),('tramnguyen', 
  '$2a$10$SPqDSFB68ejqL/sVVMeHduHa33LzCsHu7dlN/gt.52T2loQ33EQTu', 
  null, 
  'EMPLOYER'
  ),('ngantran', 
  '$2a$10$HMA5HfPnoSEAo.fFm96s2OVknjXOcaOjPPZYNH1O73u18LVDqlS1e', 
  null, 
  'EMPLOYER'
  ),('huynguyen', 
  '$2a$10$uEl8y.d.9Gf1wHyBaGTG8urlsQJI6yQnWziC.74a3cuKCzS16C22q', 
  null, 
  'EMPLOYER'
  ),('huongnguyen', 
  '$2a$10$cYDpQCu387OTrhQavKzr9.id2rRIT2itP5qJvaV9N0NS8ZyBy8k/m', 
  null, 
  'EMPLOYER'
  );
  
  
--
-- Dumping data for table `profile`
--

INSERT INTO `profile` ( `user_id`, `email`, `password`, `maso`, `phone_number`, `full_name`, `study_start`, `study_end`, `picture`, `role`, `current_semester`) VALUES
( 1, 'phamthanhvien2006@gmail.com', '123456', '51003959', '0974252080', 'Phạm Thành Viên', '2017-08-20', '2018-01-20', '', 'Student', 171),
(2, 'vienpt200807@gmail.com', '123456', '51003960', NULL, 'Phạm Văn Bình', '2010-09-01', '2017-12-10', NULL, 'Student', 171),
(3, 'admin@gmail.com', '123456', '51003959', '0909090909', 'Admin Admin', '2017-11-11', '2017-11-11', '', 'Teacher', 171),
(4, 'test1@gmail.com', '123456', '456789', '0974252080', 'Phạm Thành vũ', '2017-08-20', '2018-01-20', '', 'Student', 171),
(5, 'vxn@gmail.com', '123456', '', '', 'Vxn', '2017-11-05', '2017-11-05', '', 'Student', 0),
(6, 'annguyen@mail.com', '123456', '', '', 'Nguyễn An', '2017-11-08', '2017-11-08', '', 'Student', 0);

--
-- insert data `project`
--

INSERT INTO `project` ( `name`, `evaluate_content`, `score`, `code_id`, `created_by`, `description`, `created_at`, `updated_at`, `time_start`, `time_end`, `main_guide`) VALUES
( 'Nice schedule', 'Chưa tốt lắm ', 7.5, '1234', 3, 'Ứng dụng lịch', '2017-10-31 18:17:28', 'Fri Nov 24 15:47:14 GMT+07:00 2017', '11/11/2017', '16/2/2018', 0),
( 'Đề tài quản lý thư viện', NULL, 0, '1', 3, 'Quản lý thư viện của trường đại học Bách Khoa', '2017-10-31 18:17:54', '2017-11-20 23:34:58', '20/11/2017', '23/3/2018', 0),
( 'Đề tài quản lý phòng mạch', 'ok', 0, '1', 3, 'Quảng lý phòng khám tư', '2017-10-31 18:17:54', '2017-10-31 18:17:54', NULL, NULL, 0),
( 'ĐỀ TÀI QUẢN LÝ NHÀ SÁCH', '', 0, '1', 3, 'QUẢN LÝ NHÀ SÁCH THÀNH PHỐ', '2017-10-31 18:17:54', 'Mon Nov 20 22:52:52 GMT+07:00 2017', 'null', 'null', 0),
( 'Đề tài quảng lý khách sạn', NULL, 0, '', 1, 'Quản lý các hoạt động của khách sạn', 'Sat Nov 04 18:39:52 GMT+07:00 2017', 'Sat Nov 04 18:39:52 GMT+07:00 2017', '', '', 0),
( 'Đề tài quản lý chuyến bay', NULL, 0, '', 3, 'Quản lý lịch cũng như các hoạt động của chuyến bay.', 'Sun Nov 05 15:56:55 GMT+07:00 2017', 'Sun Nov 05 15:56:55 GMT+07:00 2017', '', '', 0),
( 'Đề tài quản lý học sinh trường trung học phổ thông.', '', 0, '', 3, 'Quản lý học sinh', 'Sun Nov 05 15:56:55 GMT+07:00 2017', 'Mon Nov 20 22:05:34 GMT+07:00 2017', '', '', 0),
( 'Đề tài quản lý giải bóng đá sinh viên toàn thành phố.', 'Chưa đạt ', 4, '', 3, '', 'Tue Nov 07 11:07:59 GMT+07:00 2017', 'Mon Nov 20 22:54:52 GMT+07:00 2017', '', '', 0),
( 'Đề tài quản lý Gara oto ', 'Tương đối', 8.5, '', 3, 'Quảng lý hệ thống sửa chửa của một gara oto', 'Sun Nov 12 21:09:25 GMT+07:00 2017', 'Fri Nov 24 15:46:17 GMT+07:00 2017', '', '', 0);

--
-- Dumping data for table `meeting`
--

INSERT INTO `meeting` ( `name`, `time`, `date`, `location`, `content`, `project_id`, `created_by`, `created_at`, `updated_at`, `unread`) VALUES
('Gặp tuần 1', '09:00', '18/11/2017', 'Phòng tiếp SV', 'Giao đề tài', 1, 3, '2017-11-11 15:42:43', '2017-11-18 09:37:49', 0),
('Gặp tuần 2', '10:00', '11/11/2017', 'Phòng tiếp SV', 'Phân tích hệ thiết kế hệ thống', 1, 3, '2017-11-11 15:42:43', '2017-11-18 09:39:47', 0),
( 'Gặp tuần 3', '15:00', '10/11/2017', 'Phòng tiếp SV', 'Thiết kế database', 1, 3, '2017-11-11 15:42:43', '2017-11-18 09:38:58', 0),
( 'Báo cáo LVTN', '10:00', '20/11/2017', '', 'Báo  cáo giữa kỳ ', 1, 3, '2017-11-17 21:45:35', '2017-11-18 09:40:05', 0);

-- --------------------------------------------------------

--
-- Dumping data for table `task`
--

INSERT INTO `task` ( `task_name`, `status`, `description`, `created_by`, `meeting_id`, `assignee`, `created_at`, `updated_at`, `due_date`, `project_id`, `note`) VALUES
( 'Đặt tả ứng dụng', 'Open', 'Mô tả tổng quan các chức năng của ứng dụng', 1, 0, 1, '2017-11-03 16:32:05', '2017-11-20 23:46:43', '3/11/2017', 1, ''),
( 'Vẽ Use case', 'Open', 'Vẽ lược đồ các chức năng cần thiết', 1, 0, 1, '2017-11-03 16:33:35', '2017-11-20 23:47:31', '10/11/2017', 1, ''),
( 'Vẽ ERD', 'Open', 'Vẽ sơ đồ liên kết giữa các thực thể trong ứng dụng', 1, 0, 1, '2017-11-03 16:32:05', '2017-11-20 23:47:49', '17/11/2017', 1, ''),
( 'Vẽ kiến trúc hệ thống', 'Open', 'Vẽ mô tả tổng quan kiến trúc hệ thống của ứng dụng', 1, 0, 1, '2017-11-03 16:32:05', '2017-11-20 23:48:03', '24/11/2017', 1, ''),
( 'Tạo giao diện login, sign up', '', 'Tạo giao diện login, sign up', 3, 0, 1, 'Sun Nov 12 17:26:00 GMT+07:00 2017', '2017-11-21 10:12:11', '12/11/2017', 2, ''),
( 'Tạo giao diện màn hình chính', 'Open', 'Xong báo cáo lại với trưởng nhóm ', 3, 0, 1, 'Sun Nov 12 23:13:53 GMT+07:00 2017', '2017-11-21 10:13:32', '23/11/2017', 2, '');

-- --------------------------------------------------------

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`ma_hk`, `name`, `start_date`, `end_date`) VALUES
(161, 'HK1(2016-2017)', '2016-09-01', '2017-01-01'),
(162, 'HK2(2016-2017)', '2017-01-01', '2017-05-01'),
(171, 'HK1(2017-2018)', '2017-09-01', '2018-01-01'),
(172, 'HK2(2017-2018)', '2018-01-01', '2018-05-01'),
(181, 'HK1(2018-2019)', '2018-09-01', '2019-01-01'),
(182, 'HK2(2018-2019)', '2019-01-01', '2019-05-01');


--
-- Dumping data for table `profiles_projects`
--

INSERT INTO `profiles_projects` (`profile_id`, `project_id`) VALUES
( 1, 1),
( 1, 2),
( 1, 3),
( 2, 1),
( 3, 1),
( 3, 2),
( 11, 4),
( 3, 4),
( 1, 4),
( 2, 4),
( 2, 5),
( 2, 6),
( 3, 6),
( 1, 7),
( 3, 7),
(3, 8),
( 2, 2),
( 2, 2);


--
-- Dumping data for table `contact`
--

INSERT INTO `contact` ( `profile_id`, `friend_email`, `friend_name`) VALUES
( 1, 'vienpt200807@gmail.com', 'Phạm Văn Bình'),
( 2, 'phamthanhvien2006@gmail.com', 'Phạm Thành Viên'),
( 2, 'admin@gmail.com', 'Admin Admin'),
( 3, 'vienpt200807@gmail.com', 'Phạm Văn Bình'),
( 3, 'phamthanhvien2006@gmail.com', 'Phạm Thành Viên'),
( 1, 'admin@gmail.com', 'Admin Admin'),
( 3, 'annguyen@mail.com', 'Nguyễn An'),
( 6, 'admin@gmail.com', 'Admin Admin');

-- --------------------------------------------------------

--
-- Dumping data for table `criterion`
--

INSERT INTO `criterion` (`profile_id`, `project_id`, `name`, `weight`, `score`, `note`) VALUES
( 3, 1, 'Dựa trên khối lượng công việc hoàn thành', 0, 0, ''),
( 3, 1, 'khả năng nghiên cứu vấn đề mới', 0, 0, ''),
( 3, 1, 'Khả năng giải quyết vấn đề', 0, 0, ''),
( 1, 1, ' khả năng làm việc nhóm', 0, 0, ''),
( 1, 1, ' khả năng trình bày', 0, 0, ''),
( 1, 1, 'Thái độ làm việc', 0, 0, '');




