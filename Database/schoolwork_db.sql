-- Xóa csdl cũ
DROP TABLE IF EXISTS
document,
comment,
contact,
criterion,
profiles_meetings,
profiles_projects,
profiles_semesters,
profiles_tasks,
projects_semesters,
semester,
task,
meeting,
project,
profile,
users;

-- bảng Users
CREATE TABLE `users` (
  id INT(11) PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(50) UNIQUE NOT NULL,
  password VARCHAR(100),
  last_password_reset DATE,
  authorities VARCHAR(20)
)CHARACTER SET utf8 COLLATE utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `profile_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `maso` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `study_start` date DEFAULT NULL,
  `study_end` date DEFAULT NULL,
  `picture` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `current_semester` int(11) DEFAULT NULL,
  FOREIGN KEY(user_id) REFERENCES users(id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;


--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `project_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `evaluate_content` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score` float NOT NULL DEFAULT '0',
  `code_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_start` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_end` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_guide` int(11) NOT NULL
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `meeting`
--
CREATE TABLE `meeting` (
  `meeting_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `unread` tinyint(1) NOT NULL DEFAULT '0',
  FOREIGN KEY (project_id) REFERENCES project(project_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;


--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `task_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `meeting_id` int(11) DEFAULT '0',
  `assignee` int(11) DEFAULT '0',
  `created_at` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `due_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `note` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  FOREIGN KEY (project_id) REFERENCES project(project_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `ma_hk` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `projects_semesters`
--

CREATE TABLE `projects_semesters` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `ma_hk` int(11) NOT NULL,
  FOREIGN KEY (project_id) REFERENCES project(project_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;


-- --------------------------------------------------------
--
-- Table structure for table `profiles_tasks`
--

CREATE TABLE `profiles_tasks` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  FOREIGN KEY (profile_id) REFERENCES profile(profile_id),
  FOREIGN KEY (task_id) REFERENCES task(task_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `profiles_semesters`
--

CREATE TABLE `profiles_semesters` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `ma_hk` int(11) NOT NULL,
  FOREIGN KEY (profile_id) REFERENCES profile(profile_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `profiles_projects`
--

CREATE TABLE `profiles_projects` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  FOREIGN KEY (profile_id) REFERENCES profile(profile_id),
  FOREIGN KEY (project_id) REFERENCES project(project_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `profiles_meetings`
--

CREATE TABLE `profiles_meetings` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  FOREIGN KEY (profile_id) REFERENCES profile(profile_id),
  FOREIGN KEY (meeting_id) REFERENCES meeting(meeting_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `criterion`
--

CREATE TABLE `criterion` (
  `criterion_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  `score` float NOT NULL DEFAULT '0',
  `note` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
   FOREIGN KEY (profile_id) REFERENCES profile(profile_id),
   FOREIGN KEY (project_id) REFERENCES project(project_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `profile_id` int(10) DEFAULT '0',
  `friend_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `friend_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  FOREIGN KEY(profile_id) REFERENCES profile(profile_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `comment_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `content` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (profile_id) REFERENCES profile(profile_id),
  FOREIGN KEY (task_id) REFERENCES task(task_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- --------------------------------------------------------
--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `document_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `content` longblob NOT NULL,  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  FOREIGN KEY (project_id) REFERENCES project(project_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;


