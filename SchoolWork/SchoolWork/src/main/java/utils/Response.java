package utils;

public class Response {
	
	private String status;
	private String value;
	
	public Response(String status, String value) {
		super();
		this.status = status;
		this.value = value;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
