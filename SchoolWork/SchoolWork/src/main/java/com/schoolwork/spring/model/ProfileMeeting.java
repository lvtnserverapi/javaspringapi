package com.schoolwork.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PROFILES_MEETINGS")
public class ProfileMeeting {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	public int getId() {
		return id;
	}

	@Column(name = "profile_id")
	private int profileId;
	
	@Column(name = "meeting_id")
	private int meetingId;
	
	

	@Column(name = "flag_read")
	private int flagRead;
	
	@Column(name = "flag_join")
	private int flagJoin;
	
	public void setId(int id) {
		this.id = id;
	}

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	public int getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}
	
	public int getFlagRead() {
		return flagRead;
	}

	public void setFlagRead(int flagRead) {
		this.flagRead = flagRead;
	}

	public int getFlagJoin() {
		return flagJoin;
	}

	public void setFlagJoin(int flagJoin) {
		this.flagJoin = flagJoin;
	}

}
