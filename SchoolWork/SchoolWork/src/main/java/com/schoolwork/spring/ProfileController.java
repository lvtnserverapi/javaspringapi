package com.schoolwork.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.schoolwork.spring.model.Classification;
import com.schoolwork.spring.model.Criterion;
import com.schoolwork.spring.model.Profile;
import com.schoolwork.spring.service.ProfileService;

@Controller
public class ProfileController {

	private ProfileService profileService;

	@Autowired(required = true)
	@Qualifier(value = "profileService")
	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}

	@RequestMapping(value = "/profiles", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Profile> listProfiles() {

		List<Profile> profileList = new ArrayList<Profile>();
		profileList = this.profileService.listProfiles();

		return profileList;

	}

	// For register or update profile
	@RequestMapping(value = "/profile/update", method = RequestMethod.POST)
	public @ResponseBody String registerOrUpdateProfile(@RequestBody Profile p) {
		String messageResponse = "";
		List<Profile> profiles = new ArrayList<Profile>();
		profiles = profileService.listProfiles();
		if (p.getId() == 0) {
			boolean isCanAdd = true;
			for (Profile profile : profiles) {

				if (p.getEmail().equals(profile.getEmail())) {// check duplicated email
					messageResponse = "Duplicated Email";
					isCanAdd = false;
					break;
				} else {
					isCanAdd = true;
				}
			}
			if (isCanAdd) {
				// new profile, add it
				this.profileService.addProfile(p);
				messageResponse = "Created";
			}

		} else {// existing profile, call update
			boolean isCanUpdate = true;
			for (Profile profile : profiles) {
				if (profile.getId() != p.getId()) {// skip me
					if (p.getEmail().equals(profile.getEmail())) {// check duplicated email
						messageResponse = "Duplicated Email";
						isCanUpdate = false;
						break;
					} else {
						isCanUpdate = true;
					}
				}
			}
			if (isCanUpdate) {
				this.profileService.updateProfile(p);

				messageResponse = "Updated";
			}
		}
		return messageResponse;
	}
	// For register
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public @ResponseBody String register(@RequestBody Profile p) {
		String messageResponse = "";
		List<Profile> profiles = new ArrayList<Profile>();
		profiles = profileService.listProfiles();
		if (p.getId() == 0) {
			boolean isCanAdd = true;
			for (Profile profile : profiles) {

				if (p.getEmail().equals(profile.getEmail())) {// check duplicated email
					messageResponse = "Duplicated Email";
					isCanAdd = false;
					break;
				} else {
					isCanAdd = true;
				}
			}
			if (isCanAdd) {
				// new profile, add it
				this.profileService.addProfile(p);
				messageResponse = "Created";
			}

		} else {// existing profile, call update
			boolean isCanUpdate = true;
			for (Profile profile : profiles) {
				if (profile.getId() != p.getId()) {// skip me
					if (p.getEmail().equals(profile.getEmail())) {// check duplicated email
						messageResponse = "Duplicated Email";
						isCanUpdate = false;
						break;
					} else {
						isCanUpdate = true;
					}
				}
			}
			if (isCanUpdate) {
				this.profileService.updateProfile(p);

				messageResponse = "Updated";
			}
		}
		return messageResponse;
	}

	@RequestMapping("/remove/{id}")
	public String removeProfile(@PathVariable("id") int id) {

		this.profileService.removeProfile(id);
		return "redirect:/profiles";
	}

	@RequestMapping("/edit/{id}")
	public String editProfile(@PathVariable("id") int id, Model model) {
		model.addAttribute("profile", this.profileService.getProfileById(id));
		model.addAttribute("listProfiles", this.profileService.listProfiles());
		return "profile";
	}

	// PROFILE - CRITERIA
	// get all criteria
	@RequestMapping(value = "/criteria", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Criterion> listCriteria() {

		List<Criterion> criteriaList = new ArrayList<Criterion>();
		criteriaList = this.profileService.listCriteria();

		return criteriaList;

	}

	// get all criteria belong to profile id
	@RequestMapping(value = "profile/criteria/{profile_id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Criterion> getCriteriaBelongToProfile(@PathVariable("profile_id") int id) {

		List<Criterion> criteriaList = new ArrayList<Criterion>();
		List<Criterion> criteriaBelongToProfile = new ArrayList<Criterion>();
		criteriaList = this.profileService.listCriteria();
		if (criteriaList != null && criteriaList.size() > 0) {
			for (Criterion criterion : criteriaList) {
				if (criterion.getProfileId() == id) {
					criteriaBelongToProfile.add(criterion);
				}
			}
		}
		return criteriaBelongToProfile;
	}

	// //create or update criterion
	// @RequestMapping(value = "/criterion/add-or-update", method =
	// RequestMethod.POST)
	// public @ResponseBody String addOrUpdateCriterion(@RequestBody Criterion
	// criterionRequest) {
	// String messageRespone = "";
	// List<Criterion> criteriaList = this.profileService.listCriteria();
	// if(criterionRequest!=null) {
	// if( criterionRequest.getId()==0) {//create criterion
	// this.profileService.addCriterion(criterionRequest);
	// messageRespone = "Created";
	// }else {//update criterion
	// if(criteriaList!=null && criteriaList.size()>0) {
	// for(Criterion criterion : criteriaList) {
	// if(criterion.getId()==criterionRequest.getId()) {
	// this.profileService.updateCriterion(criterionRequest);
	// messageRespone = "Updated";
	// }
	// }
	// }
	// }
	// }else {
	// messageRespone = "Request Null";
	// }
	//
	// return messageRespone;
	// }

	// create or update list criteria
	@RequestMapping(value = "/criterion/add-or-update", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdateCriterion(@RequestBody List<Criterion> criteriaRequest) {
		String messageRespone = "";
		List<Criterion> criteriaList = this.profileService.listCriteria();
		if (criteriaRequest != null && criteriaRequest.size() > 0) {
			// xoa cac tieu chi chung khong ton tai o local
			int idProfileRequest = criteriaRequest.get(0).getProfileId();
			int idProjectRequest = criteriaRequest.get(0).getProjectId();
			if (idProjectRequest == 0) {
				for (Criterion criterionServer : criteriaList) {
					// lọc lấy những tiêu chí chung của profile
					if (criterionServer.getProfileId() == idProfileRequest && criterionServer.getProjectId() == 0) {
						boolean isRemove = true;
						for (Criterion criterion : criteriaRequest) {// kiem tra neu ton tai o local se khong xoa
							if (criterionServer.getId() == criterion.getId()) {
								isRemove = false;
							}
						}
						if (isRemove) {// xoa neu khong ton tai o local
							String nameCriterionRemoved = criterionServer.getName();
							int profileIdOfCriterionRemoved = criterionServer.getProfileId();
							this.profileService.removeCriterion(criterionServer.getId());
							criteriaList = this.profileService.listCriteria();
							// xoa tieu chi da duoc xoa trong phan chung o cac project khac
							for (Criterion criterion2 : criteriaList) {
								if (criterion2.getProfileId() == profileIdOfCriterionRemoved
										&& criterion2.getName().equals(nameCriterionRemoved)) {
									this.profileService.removeCriterion(criterion2.getId());
									messageRespone = "Removed it in others";
								}
							}
						}
					}
				}
			}

			criteriaList = this.profileService.listCriteria();

			for (Criterion criterion : criteriaRequest) {// duyệt danh sách tiêu chí request
				if (criterion != null) {
					if (criterion.getId() == 0) {// tạo tiêu chí mới cho profile giành cho setting
						this.profileService.addCriterion(criterion);
						// thuc hien them tieu chi moi cho cac project da duoc danh gia it nhat 1 lan
						List<Integer> projectIdList = new ArrayList<Integer>();
						projectIdList.add(0);
						for (Criterion criterionData : criteriaList) {
							// kiem tra neu tieu chi moi da duoc them cho project thi bo qua
							if (projectIdList.contains(criterionData.getProjectId())) {
								continue;
							} else {
								if (criterionData.getProfileId() == criterion.getProfileId()) {
									Criterion c = new Criterion();
									c.setId(0);
									c.setProfileId(criterion.getProfileId());
									c.setProjectId(criterionData.getProjectId());
									c.setName(criterion.getName());
									c.setWeight(criterion.getWeight());
									c.setScore(0);
									c.setNote(criterion.getNote());
									this.profileService.addCriterion(c);
									projectIdList.add(c.getProjectId());
								}
							}
						}
						messageRespone = "Created setting";
					} else if (criterion.getProjectId() == 0 && criteriaList != null && criteriaList.size() > 0) {
						// cập nhật thông tin các tiêu chí chung trong trường hợp danh sách tiêu chí
						// request là các tiêu chí chung có projectId bằng 0
						for (Criterion criterion3 : criteriaList) {
							if (criterion3.getId() == criterion.getId()) {
								this.profileService.updateCriterion(criterion);
								messageRespone = "Updated setting";
							}
						}
					} else {
						// kiểm tra xem có phải project được đánh giá lần đâu tiên hay không giành cho
						// đánh giá
						boolean isFirstTime = true;
						for (Criterion criterion4 : criteriaList) {
							if (criterion4.getProfileId() == criterion.getProfileId()
									&& criterion4.getProjectId() == criterion.getProjectId()) {
								isFirstTime = false;
							}
						}
						// trong trường hợp danh sách tiêu chí request là của một project cụ thể được
						// đánh giá lần đầu
						if (criterion.getProjectId() != 0 && isFirstTime) {
							Criterion newCriterion = new Criterion();
							newCriterion.setId(0);
							newCriterion.setProfileId(criterion.getProfileId());
							newCriterion.setProjectId(criterion.getProjectId());
							newCriterion.setName(criterion.getName());
							newCriterion.setWeight(criterion.getWeight());
							newCriterion.setScore(criterion.getScore());
							newCriterion.setNote(criterion.getNote());
							this.profileService.addCriterion(newCriterion);
							messageRespone = "Created First Time";
						} else {// cập nhật tiêu chí đã đánh giá
							this.profileService.updateCriterion(criterion);
							messageRespone = "Updated";
						}
					}
				} else {
					messageRespone = "Request Null";
				}
			}
		}
		return messageRespone;
	}

	// reset criteria for project
	@RequestMapping(value = "project/criteria/reset/{profile_id}/{project_id}", method = RequestMethod.POST)
	public @ResponseBody String resetCriteria(@PathVariable("profile_id") int profile_id,
			@PathVariable("project_id") int project_id) {
		String messageRespone = "";
		List<Criterion> criteriaList = this.profileService.listCriteria();
		if (criteriaList != null && criteriaList.size() > 0) {
			boolean isEvaluated = false;
			for (Criterion criterion : criteriaList) {
				if (criterion.getProfileId() == profile_id && criterion.getProjectId() == project_id) {
					this.profileService.removeCriterion(criterion.getId());
					isEvaluated = true;
				}
			}
			if (isEvaluated) {
				messageRespone = "Removed";
			} else {
				messageRespone = "Not existed";
			}
		}
		return messageRespone;
	}

	// PROFILE - CLASSIFICATIONS,
	// get all classifications
	@RequestMapping(value = "/classifications", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Classification> listclassifications() {

		List<Classification> classificationsList = new ArrayList<Classification>();
		classificationsList = this.profileService.listClassifications();

		return classificationsList;

	}

	// get classifications belong to profile
	@RequestMapping(value = "/profile/classifications/{profile_id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Classification> getClassificationsBelongToProfile(@PathVariable("profile_id") int id) {
		List<Classification> classificationsBelongToProfile = new ArrayList<Classification>();
		List<Classification> classifications = this.profileService.listClassifications();
		if (null != classifications && classifications.size() > 0) {
			for (Classification classification : classifications) {
				if (classification.getProfileId() == id) {
					classificationsBelongToProfile.add(classification);
				}
			}
		}
		return classificationsBelongToProfile;
	}

	// remove classification
	@RequestMapping(value = "/classification/remove/{classification_id}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public @ResponseBody String removeClassification(@PathVariable("classification_id") int classification_id) {
		List<Classification> classifications = this.profileService.listClassifications();
		if (null != classifications && classifications.size() > 0) {
			for (Classification classification : classifications) {
				if (classification.getId() == classification_id) {
					classifications.remove(classification.getId());
					return "removed";
				}
			}
		}
		return null;
	}

	// add or update classification
	@RequestMapping(value = "/classifications/add-or-update/{profile_id}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public @ResponseBody String addOrUpdateClassification(@RequestBody List<Classification> classificationsRequest,
			@PathVariable("profile_id") int profileId) {
		String messageRespone = "";
		List<Classification> classifications = this.profileService.listClassifications();

		if (classificationsRequest != null && classificationsRequest.size() > 0) {
			// xoa cac classification cua profile
			if (null != classifications && classifications.size() > 0) {
				for (Classification classification : classifications) {
					if (classification.getProfileId() == profileId) {
						this.profileService.removeClassification(classification.getId());
					}
				}
			}

			for (Classification classificationRequest : classificationsRequest) {
				this.profileService.addClassification(classificationRequest);
				messageRespone = "Updated";
			}
		} else if (classificationsRequest.size() == 0) {// trường hợp giáo viên muốn xóa hết tất cả các classification
			if (null != classifications && classifications.size() > 0) {
				for (Classification classification : classifications) {
					if (classification.getProfileId() == profileId) {
						this.profileService.removeClassification(classification.getId());
						messageRespone = "Removed all";
					}
				}
			}
		}

		return messageRespone;
	}
}
