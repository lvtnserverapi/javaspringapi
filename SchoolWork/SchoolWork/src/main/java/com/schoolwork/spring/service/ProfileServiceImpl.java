package com.schoolwork.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.schoolwork.spring.dao.ProfileDAO;
import com.schoolwork.spring.model.Classification;
import com.schoolwork.spring.model.Criterion;
import com.schoolwork.spring.model.Meeting;
import com.schoolwork.spring.model.Profile;
import com.schoolwork.spring.model.ProfileMeeting;
import com.schoolwork.spring.model.Task;

@Service
public class ProfileServiceImpl implements ProfileService {

	private ProfileDAO profileDAO;

	public void setProfileDAO(ProfileDAO profileDAO) {
		this.profileDAO = profileDAO;
	}

	@Override
	@Transactional
	public void addProfile(Profile profile) {
		this.profileDAO.addProfile(profile);
	}

	@Override
	@Transactional
	public void updateProfile(Profile profile) {
		this.profileDAO.updateProfile(profile);
	}

	@Override
	@Transactional
	public List<Profile> listProfiles() {
		return this.profileDAO.listProfiles();
	}

	@Override
	@Transactional
	public Profile getProfileById(int id) {
		return this.profileDAO.getProfileById(id);
	}

	@Override
	@Transactional
	public void removeProfile(int id) {
		this.profileDAO.removeProfile(id);
	}

	// PROJECT - TASKS
	@Override
	@Transactional
	public void addTask(Task task) {
		this.profileDAO.addTask(task);
	}

	@Override
	@Transactional
	public void updateTask(Task task) {
		this.profileDAO.updateTask(task);
	}

	@Override
	@Transactional
	public List<Task> listTasks() {
		return this.profileDAO.listTasks();
	}

	@Override
	@Transactional
	public Task getTaskById(int id) {
		return this.profileDAO.getTaskById(id);
	}

	@Override
	@Transactional
	public void removeTask(int id) {
		this.profileDAO.removeTask(id);
	}

	// PROFILE- MEETINGS

	@Override
	@Transactional
	public void addMeeting(Meeting meeting) {
		this.profileDAO.addMeeting(meeting);
	}

	@Override
	@Transactional
	public void updateMeeting(Meeting meeting) {
		this.profileDAO.updateMeeting(meeting);
	}

	@Override
	@Transactional
	public List<Meeting> listMeetings() {
		return this.profileDAO.listMeetings();
	}

	@Override
	@Transactional
	public Meeting getMeetingById(int id) {
		return this.profileDAO.getMeetingById(id);
	}

	@Override
	@Transactional
	public void removeMeeting(int id) {
		this.profileDAO.removeMeeting(id);
	}
	
	// PROFILESMEETINGS
	@Override
	@Transactional
	public void addProfileMeeting(ProfileMeeting profileMeeting) {
		this.profileDAO.addProfileMeeting(profileMeeting);
	}

	@Override
	@Transactional
	public void updateProfileMeeting(ProfileMeeting profileMeeting) {
		this.profileDAO.updateProfileMeeting(profileMeeting);
	}

	@Override
	@Transactional
	public List<ProfileMeeting> listProfileMeetings() {
		return this.profileDAO.listProfileMeetings();
	}

	@Override
	@Transactional
	public void removeProfileMeeting(int id) {
		this.profileDAO.removeProfileMeeting(id);
	}

	// PROJECT- CRITERIA

	@Override
	@Transactional
	public void addCriterion(Criterion p) {
		this.profileDAO.addCriterion(p);
	}

	@Override
	@Transactional
	public void updateCriterion(Criterion criterion) {
		this.profileDAO.updateCriterion(criterion);
	}

	@Override
	@Transactional
	public List<Criterion> listCriteria() {
		return this.profileDAO.listCriteria();
	}

	@Override
	@Transactional
	public Criterion getCriterionById(int id) {
		return this.profileDAO.getCriterionById(id);
	}

	@Override
	@Transactional
	public void removeCriterion(int id) {
		this.profileDAO.removeCriterion(id);
	}

	//// PROFILE- CLASSIFICATIONS

	@Override
	@Transactional
	public void addClassification(Classification classification) {
		this.profileDAO.addClassification(classification);
	}

	@Override
	@Transactional
	public void updateClassification(Classification classification) {
		this.profileDAO.updateClassification(classification);
	}

	@Override
	@Transactional
	public List<Classification> listClassifications() {
		return this.profileDAO.listClassifications();
	}

	@Override
	@Transactional
	public void removeClassification(int id) {
		this.profileDAO.removeClassification(id);
	}

}
