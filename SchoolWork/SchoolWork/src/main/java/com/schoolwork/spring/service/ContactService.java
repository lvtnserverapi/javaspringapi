package com.schoolwork.spring.service;

import java.util.List;

import com.schoolwork.spring.model.Contact;
import com.schoolwork.spring.model.Profile;

public interface ContactService {
	
	public void addContact(Contact contact);
	public void updateContact(Contact contact);
	public List<Contact> listContacts();
	public Contact getContactById(int id);
	public void removeContact(int id);
	
	public List<Profile> listProfiles();

}
