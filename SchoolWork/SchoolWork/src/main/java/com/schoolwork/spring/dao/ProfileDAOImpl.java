package com.schoolwork.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.schoolwork.spring.model.Classification;
import com.schoolwork.spring.model.Criterion;
import com.schoolwork.spring.model.Meeting;
import com.schoolwork.spring.model.Profile;
import com.schoolwork.spring.model.ProfileMeeting;
import com.schoolwork.spring.model.Task;

@Repository
public class ProfileDAOImpl implements ProfileDAO {

	// private static final Logger logger =
	// LoggerFactory.getLogger(ProfileDAOImpl.class);
	// @Autowired
	// JdbcTemplate jdbcTemplate;

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addProfile(Profile profile) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(profile);
	}

	@Override
	public void updateProfile(Profile profile) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(profile);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Profile> listProfiles() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Profile> profilesList = session.createQuery("from Profile").list();
		return profilesList;
	}

	@Override
	public Profile getProfileById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Profile profile = (Profile) session.load(Profile.class, new Integer(id));
		return profile;
	}

	@Override
	public void removeProfile(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Profile profile = (Profile) session.load(Profile.class, new Integer(id));
		if (null != profile) {
			session.delete(profile);
		}
	}

	// PROFILES - TASKS
	@Override
	public void addTask(Task task) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(task);
	}

	@Override
	public void updateTask(Task task) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(task);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> listTasks() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Task> tasksList = session.createQuery("from Task").list();
		return tasksList;
	}

	@Override
	public Task getTaskById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Task task = (Task) session.load(Task.class, new Integer(id));
		return task;
	}

	@Override
	public void removeTask(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Task task = (Task) session.load(Task.class, new Integer(id));
		if (null != task) {
			session.delete(task);
		}
	}

	// PROFILES - MEETINGS
	@Override
	public void addMeeting(Meeting meeting) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(meeting);
	}

	@Override
	public void updateMeeting(Meeting meeting) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(meeting);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Meeting> listMeetings() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Meeting> meetingsList = session.createQuery("from Meeting").list();
		return meetingsList;
	}

	@Override
	public Meeting getMeetingById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Meeting meeting = (Meeting) session.load(Meeting.class, new Integer(id));
		return meeting;
	}

	@Override
	public void removeMeeting(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Meeting meeting = (Meeting) session.load(Meeting.class, new Integer(id));
		if (null != meeting) {
			session.delete(meeting);
		}
	}

	// PROFILESMEETINGS
	@Override
	public void addProfileMeeting(ProfileMeeting profileMeeting) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(profileMeeting);
	}

	@Override
	public void updateProfileMeeting(ProfileMeeting profileMeeting) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(profileMeeting);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProfileMeeting> listProfileMeetings() {
		Session session = this.sessionFactory.getCurrentSession();
		List<ProfileMeeting> profileMeetingsList = session.createQuery("from ProfileMeeting").list();
		return profileMeetingsList;
	}

	@Override
	public void removeProfileMeeting(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ProfileMeeting profileMeeting = (ProfileMeeting) session.load(ProfileMeeting.class, new Integer(id));
		if (null != profileMeeting) {
			session.delete(profileMeeting);
		}
	}

	// PROFILE - CRITERIA
	@Override
	public void addCriterion(Criterion criterion) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(criterion);
	}

	@Override
	public void updateCriterion(Criterion criterion) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(criterion);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Criterion> listCriteria() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Criterion> criteriaList = session.createQuery("from Criterion").list();
		return criteriaList;
	}

	@Override
	public Criterion getCriterionById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criterion criterion = (Criterion) session.load(Criterion.class, new Integer(id));
		return criterion;
	}

	@Override
	public void removeCriterion(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criterion criterion = (Criterion) session.load(Criterion.class, new Integer(id));
		if (null != criterion) {
			session.delete(criterion);
		}
	}

	// PROFILE - CLASSIFICATIONS
	@Override
	public void addClassification(Classification classification) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(classification);
	}

	@Override
	public void updateClassification(Classification classification) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(classification);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Classification> listClassifications() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Classification> classificationList = session.createQuery("from Classification").list();
		return classificationList;
	}

	@Override
	public void removeClassification(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Classification classification = (Classification) session.load(Classification.class, new Integer(id));
		if (null != classification) {
			session.delete(classification);
		}
	}

}
