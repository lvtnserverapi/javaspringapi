package com.schoolwork.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.schoolwork.spring.model.Comment;
import com.schoolwork.spring.service.CommentService;

@Controller
public class CommentController {
	private CommentService commentService;

	@Autowired(required = true)
	@Qualifier(value = "commentService")
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	@RequestMapping(value = "/comments", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Comment> listComments() {

		List<Comment> commentList = new ArrayList<Comment>();
		commentList = this.commentService.listComments();

		return commentList;

	}
}
