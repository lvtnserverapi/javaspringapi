package com.schoolwork.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "classification")
public class Classification {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "classification_id")
	private int id;

	@Column(name = "profile_id")
	private int profileId;

	@Column(name = "name")
	private String name;

	@Column(name = "from_score")
	private float fromScore;

	@Column(name = "to_score")
	private float toScore;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getFromScore() {
		return fromScore;
	}

	public void setFromScore(float fromScore) {
		this.fromScore = fromScore;
	}

	public float getToScore() {
		return toScore;
	}

	public void setToScore(float toScore) {
		this.toScore = toScore;
	}

}
