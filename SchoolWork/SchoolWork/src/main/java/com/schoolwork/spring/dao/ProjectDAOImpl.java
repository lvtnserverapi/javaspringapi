package com.schoolwork.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.schoolwork.spring.model.Meeting;
import com.schoolwork.spring.model.Notification;
import com.schoolwork.spring.model.ProfileMeeting;
import com.schoolwork.spring.model.ProfileProject;
import com.schoolwork.spring.model.Project;
import com.schoolwork.spring.model.Task;

@Repository
public class ProjectDAOImpl implements ProjectDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addProject(Project project) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(project);
	}

	@Override
	public void updateProject(Project project) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(project);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> listProjects() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Project> projectsList = session.createQuery("from Project").list();
		return projectsList;
	}

	@Override
	public Project getProjectById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Project project = (Project) session.load(Project.class, new Integer(id));
		return project;
	}

	@Override
	public void removeProject(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Project project = (Project) session.load(Project.class, new Integer(id));
		if (null != project) {
			session.delete(project);
		}
	}

	// PROJECTS - PROFILES
	@Override
	public void removeProfileProject(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ProfileProject profileProject = (ProfileProject) session.load(ProfileProject.class, new Integer(id));
		if (null != profileProject) {
			session.delete(profileProject);
		}
	}

	@Override
	public void addProfileProject(ProfileProject profileProject) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(profileProject);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProfileProject> getProfileProjects() {
		Session session = this.sessionFactory.getCurrentSession();
		List<ProfileProject> ProfileProjectsList = session.createQuery("from ProfileProject").list();

		return ProfileProjectsList;
	}

	// PROJECT-TASKS
	@Override
	public void addTask(Task task) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(task);
	}

	@Override
	public void updateTask(Task task) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(task);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> listTasks() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Task> tasksList = session.createQuery("from Task").list();

		return tasksList;
	}

	@Override
	public Task getTaskById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Task task = (Task) session.load(Task.class, new Integer(id));
		return task;
	}

	@Override
	public void removeTask(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Task task = (Task) session.load(Task.class, new Integer(id));
		if (null != task) {
			session.delete(task);
		}
	}

	// PROJECT - MEETINGS
	@Override
	public void addMeeting(Meeting meeting) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(meeting);
	}

	@Override
	public void updateMeeting(Meeting meeting) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(meeting);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Meeting> listMeetings() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Meeting> meetingsList = session.createQuery("from Meeting").list();
		return meetingsList;
	}

	@Override
	public Meeting getMeetingById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Meeting meeting = (Meeting) session.load(Meeting.class, new Integer(id));
		return meeting;
	}

	@Override
	public void removeMeeting(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Meeting meeting = (Meeting) session.load(Meeting.class, new Integer(id));
		if (null != meeting) {
			session.delete(meeting);
		}
	}
	
	//PROJECT-NOTIFICATIONS
	@Override
	public void addNotification(Notification notification) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(notification);
	}

	@Override
	public void updateNotification(Notification notification) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(notification);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> listNotifications() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Notification> notificationsList = session.createQuery("from Notification").list();
		return notificationsList;
	}

	@Override
	public void removeNotification(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Notification notification = (Notification) session.load(Notification.class, new Integer(id));
		if (null != notification) {
			session.delete(notification);
		}
	}
	
	// PROFILESMEETINGS
		@Override
		public void addProfileMeeting(ProfileMeeting profileMeeting) {
			Session session = this.sessionFactory.getCurrentSession();
			session.persist(profileMeeting);
		}

		@Override
		public void updateProfileMeeting(ProfileMeeting profileMeeting) {
			Session session = this.sessionFactory.getCurrentSession();
			session.update(profileMeeting);
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<ProfileMeeting> listProfileMeetings() {
			Session session = this.sessionFactory.getCurrentSession();
			List<ProfileMeeting> profileMeetingsList = session.createQuery("from ProfileMeeting").list();
			return profileMeetingsList;
		}

		@Override
		public void removeProfileMeeting(int id) {
			Session session = this.sessionFactory.getCurrentSession();
			ProfileMeeting profileMeeting = (ProfileMeeting) session.load(ProfileMeeting.class, new Integer(id));
			if (null != profileMeeting) {
				session.delete(profileMeeting);
			}
		}

}
