package com.schoolwork.spring.dao;

import java.util.List;

import com.schoolwork.spring.model.Classification;
import com.schoolwork.spring.model.Criterion;
import com.schoolwork.spring.model.Meeting;
import com.schoolwork.spring.model.Profile;
import com.schoolwork.spring.model.ProfileMeeting;
import com.schoolwork.spring.model.Task;

public interface ProfileDAO {

	public void addProfile(Profile p);
	public void updateProfile(Profile p);
	public List<Profile> listProfiles();
	public Profile getProfileById(int id);
	public void removeProfile(int id);
	
	//TO DO
//	public void register(Profile profile);
//	public Profile validateProfile(Login login);
	
	//PROFILES - TASKS
	public void addTask(Task task);
	public void updateTask(Task task);
	public List<Task> listTasks();
	public Task getTaskById(int id);
	public void removeTask(int id);
	
	//PROFILES - MEETINGS
	public void addMeeting(Meeting meeting);
	public void updateMeeting(Meeting meeting);
	public List<Meeting> listMeetings();
	public Meeting getMeetingById(int id);
	public void removeMeeting(int id);
	//PROFILESMEETINGS
	public void addProfileMeeting(ProfileMeeting profileMeeting);
	public void updateProfileMeeting(ProfileMeeting profileMeeting);
	public List<ProfileMeeting> listProfileMeetings();
	public void removeProfileMeeting(int id);
	
	//PROFILE - CRITERIA
	public void addCriterion(Criterion criterion);
	public void updateCriterion(Criterion criterion);
	public List<Criterion> listCriteria();
	public Criterion getCriterionById(int id);
	public void removeCriterion(int id);
	
	//PROFILE - CLASSIFICATIONS
	public void addClassification(Classification classification);
	public void updateClassification(Classification classification);
	public List<Classification> listClassifications();
	public void removeClassification(int id);
}
