package com.schoolwork.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.schoolwork.spring.dao.ProjectDAO;
import com.schoolwork.spring.model.Meeting;
import com.schoolwork.spring.model.Notification;
import com.schoolwork.spring.model.ProfileMeeting;
import com.schoolwork.spring.model.ProfileProject;
import com.schoolwork.spring.model.Project;
import com.schoolwork.spring.model.Task;

@Service
public class ProjectServiceImpl implements ProjectService {

	private ProjectDAO projectDAO;

	public void setProjectDAO(ProjectDAO projectDAO) {
		this.projectDAO = projectDAO;
	}

	@Override
	@Transactional
	public void addProject(Project profject) {
		this.projectDAO.addProject(profject);
	}

	@Override
	@Transactional
	public void updateProject(Project profject) {
		this.projectDAO.updateProject(profject);
	}

	@Override
	@Transactional
	public List<Project> listProjects() {
		return this.projectDAO.listProjects();
	}

	@Override
	@Transactional
	public Project getProjectById(int id) {
		return this.projectDAO.getProjectById(id);
	}

	@Override
	@Transactional
	public void removeProject(int id) {
		this.projectDAO.removeProject(id);
	}

	// PROJECTS - PROFILES
	@Override
	@Transactional
	public void removeProfileProject(int id) {
		this.projectDAO.removeProfileProject(id);
	}

	@Override
	@Transactional
	public void addProfileProject(ProfileProject pp) {
		this.projectDAO.addProfileProject(pp);
	}

	@Override
	@Transactional
	public List<ProfileProject> getProfileProjects() {
		return this.projectDAO.getProfileProjects();
	}

	// PROJECT - TASKS
	@Override
	@Transactional
	public void addTask(Task task) {
		this.projectDAO.addTask(task);
	}

	@Override
	@Transactional
	public void updateTask(Task task) {
		this.projectDAO.updateTask(task);
	}

	@Override
	@Transactional
	public List<Task> listTasks() {
		return this.projectDAO.listTasks();
	}

	@Override
	@Transactional
	public Task getTaskById(int id) {
		return this.projectDAO.getTaskById(id);
	}

	@Override
	@Transactional
	public void removeTask(int id) {
		this.projectDAO.removeTask(id);
	}

	// PROJECT- MEETINGS

	@Override
	@Transactional
	public void addMeeting(Meeting meeting) {
		this.projectDAO.addMeeting(meeting);
	}

	@Override
	@Transactional
	public void updateMeeting(Meeting meeting) {
		this.projectDAO.updateMeeting(meeting);
	}

	@Override
	@Transactional
	public List<Meeting> listMeetings() {
		return this.projectDAO.listMeetings();
	}

	@Override
	@Transactional
	public Meeting getMeetingById(int id) {
		return this.projectDAO.getMeetingById(id);
	}

	@Override
	@Transactional
	public void removeMeeting(int id) {
		this.projectDAO.removeMeeting(id);
	}

	// PROJECT- NOTIFICATIONS
	@Override
	@Transactional
	public void addNotification(Notification notification) {
		this.projectDAO.addNotification(notification);
	}

	@Override
	@Transactional
	public void updateNotification(Notification notification) {
		this.projectDAO.updateNotification(notification);
	}

	@Override
	@Transactional
	public List<Notification> listNotifications() {
		return this.projectDAO.listNotifications();
	}


	@Override
	@Transactional
	public void removeNotification(int id) {
		this.projectDAO.removeNotification(id);
	}
	
	// PROFILESMEETINGS
	@Override
	@Transactional
	public void addProfileMeeting(ProfileMeeting profileMeeting) {
		this.projectDAO.addProfileMeeting(profileMeeting);
	}

	@Override
	@Transactional
	public void updateProfileMeeting(ProfileMeeting profileMeeting) {
		this.projectDAO.updateProfileMeeting(profileMeeting);
	}

	@Override
	@Transactional
	public List<ProfileMeeting> listProfileMeetings() {
		return this.projectDAO.listProfileMeetings();
	}

	@Override
	@Transactional
	public void removeProfileMeeting(int id) {
		this.projectDAO.removeProfileMeeting(id);
	}
}
