package com.schoolwork.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.schoolwork.spring.model.Comment;


@Repository
public class CommentDAOImpl implements CommentDAO {

//	private static final Logger logger = LoggerFactory.getLogger(CommentDAOImpl.class);
//	@Autowired
//	JdbcTemplate jdbcTemplate;
	
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addComment(Comment comment) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(comment);
//		logger.info("Comment saved successfully, Comment Details=" + comment);
	}

	@Override
	public void updateComment(Comment comment) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(comment);
//		logger.info("Comment updated successfully, Comment Details=" + comment);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> listComments() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Comment> commentsList = session.createQuery("from Comment").list();

		return commentsList;
	}

	@Override
	public Comment getCommentById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Comment comment = (Comment) session.load(Comment.class, new Integer(id));
//		logger.info("Comment loaded successfully, Comment details=" + comment);
		return comment;
	}

	@Override
	public void removeComment(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Comment comment = (Comment) session.load(Comment.class, new Integer(id));
		if (null != comment) {
			session.delete(comment);
		}
//		logger.info("Comment deleted successfully, Comment details=" + comment);
	}

}
