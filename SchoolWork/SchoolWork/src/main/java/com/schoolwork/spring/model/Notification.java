package com.schoolwork.spring.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notification")
public class Notification {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "notification_id")
	private int id;

	@Column(name = "notice_from")
	private int noticeFrom;

	@Column(name = "notice_to")
	private int noticeTo;

	@Column(name = "project_id")
	private int projectId;
	
	@Column(name = "meeting_id")
	private int meetingId;

	@Column(name = "task_id")
	private int taskId;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "created_at")
	private Timestamp createdAt;
	
	@Column(name = "flag_read")
	private int flagRead;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNoticeFrom() {
		return noticeFrom;
	}

	public void setNoticeFrom(int noticeFrom) {
		this.noticeFrom = noticeFrom;
	}

	public int getNoticeTo() {
		return noticeTo;
	}

	public void setNoticeTo(int noticeTo) {
		this.noticeTo = noticeTo;
	}
	
	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public int getFlagRead() {
		return flagRead;
	}

	public void setFlagRead(int flagRead) {
		this.flagRead = flagRead;
	}
}
