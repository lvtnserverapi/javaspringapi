package com.schoolwork.spring;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.schoolwork.spring.model.Meeting;
import com.schoolwork.spring.model.Notification;
import com.schoolwork.spring.model.ProfileMeeting;
import com.schoolwork.spring.model.ProfileProject;
import com.schoolwork.spring.model.Project;
import com.schoolwork.spring.model.ProjectCustom;
import com.schoolwork.spring.model.Task;
import com.schoolwork.spring.service.ProjectService;

import utils.Utils;

@Controller
public class ProjectController {
	private ProjectService projectService;

	@Autowired(required = true)
	@Qualifier(value = "projectService")
	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	@RequestMapping(value = "/projects", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Project> listProjects() {
		List<Project> projectList = new ArrayList<Project>();
		projectList = this.projectService.listProjects();

		return projectList;

	}

	// get projects specific profile id
	@RequestMapping(value = {
			"/projects/{profile_id}" }, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<ProjectCustom> getProjectByProfileId(@PathVariable("profile_id") int id) {
		List<Project> projectList = new ArrayList<Project>();
		List<ProfileProject> profileProjectList = new ArrayList<ProfileProject>();
		List<Project> projectListOfProfile = new ArrayList<Project>();
		projectList = this.projectService.listProjects();
		profileProjectList = this.projectService.getProfileProjects();
		for (ProfileProject pp : profileProjectList) {
			if (pp.getProfileId() == id) {
				for (Project p : projectList) {
					if (p.getId() == pp.getProjectId()) {
						projectListOfProfile.add(p);// co duoc danh sach project cua profile id
						break;
					}
				}
			}
		}

		List<ProjectCustom> projectCustoms = new ArrayList<ProjectCustom>();
		for (Project project : projectListOfProfile) {
			ProjectCustom projectCustom = new ProjectCustom();
			List<Integer> members = new ArrayList<Integer>();
			for (ProfileProject profileProject : profileProjectList) {
				if (profileProject.getProjectId() == project.getId()) {
					members.add(profileProject.getProfileId());
				}
			}
			projectCustom.setId(project.getId());
			projectCustom.setName(project.getName());
			projectCustom.setRating(project.getRating());
			projectCustom.setScore(project.getScore());
			projectCustom.setCode(project.getCode());
			projectCustom.setDescription(project.getDescription());
			projectCustom.setRequirement(project.getRequirement());
			projectCustom.setKeyword(project.getKeyword());
			projectCustom.setReference(project.getReference());
			projectCustom.setCreatedBy(project.getCreatedBy());
			projectCustom.setCreatedAt(project.getCreatedAt());
			projectCustom.setUpdatedAt(project.getUpdatedAt());
			projectCustom.setTimeStart(project.getTimeStart());
			projectCustom.setTimeEnd(project.getTimeEnd());
			projectCustom.setMainGuide(project.getMainGuide());
			Integer[] memberArr = new Integer[members.size()];
			members.toArray(memberArr);
			projectCustom.setMembers(memberArr);

			projectCustoms.add(projectCustom);
		}
		return projectCustoms;
	}

	// get profile_projects
	@RequestMapping(value = "/profile-projects", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<ProfileProject> listProfileProjects() {
		List<ProfileProject> profileProjects = new ArrayList<ProfileProject>();
		profileProjects = this.projectService.getProfileProjects();

		return profileProjects;

	}

	// create or update project
	@RequestMapping(value = "/project/add-or-update", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdateProject(@RequestBody ProjectCustom projectCustom) {
		String messageResponse = "";
		Project project = new Project();
		List<Project> projects = new ArrayList<Project>();
		projects = this.projectService.listProjects();

		// convert ProjectCustom to project
		if (projectCustom != null) {
			// add or update project
			project.setId(projectCustom.getId());
			project.setCode(projectCustom.getCode());
			project.setDescription(projectCustom.getDescription());
			project.setRequirement(projectCustom.getRequirement());
			project.setKeyword(projectCustom.getKeyword());
			project.setReference(projectCustom.getReference());
			project.setName(projectCustom.getName());
			project.setRating(projectCustom.getName());
			project.setScore(projectCustom.getScore());
			project.setCreatedBy(projectCustom.getCreatedBy());
			project.setRating(projectCustom.getRating());
			project.setCreatedAt(projectCustom.getCreatedAt());
			project.setUpdatedAt(projectCustom.getUpdatedAt());
			project.setTimeStart(projectCustom.getTimeStart());
			project.setTimeEnd(projectCustom.getTimeEnd());
			project.setMainGuide(projectCustom.getMainGuide());
			if (project.getId() == 0) {
				for (Project temp : projects) {
					if (temp.getName().equals(projectCustom.getName())) {
						return messageResponse = "Duplicated project";
					}
				}
				// new project, add it
				this.projectService.addProject(project);
				projects = this.projectService.listProjects();
				// find project id added
				int projectIdJustAdded = 0;
				for (Project tempP : projects) {
					if (tempP.getName().equals(projectCustom.getName())) {
						projectIdJustAdded = tempP.getId();
					}
				}
				// add member project
				if (projectCustom.getMembers().length > 0) {
					for (int i = 0; i < projectCustom.getMembers().length; i++) {
						ProfileProject profileProject = new ProfileProject();
						profileProject.setProfileId(projectCustom.getMembers()[i]);
						profileProject.setProjectId(projectIdJustAdded);
						this.projectService.addProfileProject(profileProject);
					}
				}
				messageResponse = "Created";

			} else {
				// existing project, call update
				this.projectService.updateProject(project);
				// add or update member
				List<ProfileProject> profileProjects = new ArrayList<ProfileProject>();
				profileProjects = this.projectService.getProfileProjects();

				List<ProfileProject> tempList = new ArrayList<ProfileProject>();
				// filter by project ID
				for (ProfileProject profileProject : profileProjects) {
					if (profileProject.getProjectId() == projectCustom.getId()) {
						tempList.add(profileProject);
					}
				}
				// remove member from project
				for (ProfileProject profileProject : tempList) {
					boolean isRemove = true;
					for (int i = 0; i < projectCustom.getMembers().length; i++) {
						if (profileProject.getProfileId() == projectCustom.getMembers()[i]) {
							isRemove = false;
							break;
						}
					}
					if (isRemove) {
						this.projectService.removeProfileProject(profileProject.getId());
					}
				}
				// add member to project
				for (int i = 0; i < projectCustom.getMembers().length; i++) {
					boolean isAdd = true;
					for (ProfileProject profileProject : tempList) {
						if (profileProject.getProfileId() == projectCustom.getMembers()[i]) {
							isAdd = false;
							break;
						}
					}
					if (isAdd) {
						ProfileProject profileProject = new ProfileProject();
						profileProject.setProfileId(projectCustom.getMembers()[i]);
						profileProject.setProjectId(projectCustom.getId());
						this.projectService.addProfileProject(profileProject);
					}
				}
				messageResponse = "Updated";
			}
		}
		return messageResponse;
	}

	// Delete project
	@RequestMapping(value = "/project/remove/{project_id}", method = RequestMethod.POST)
	public @ResponseBody String removeProject(@PathVariable("project_id") int projectId) {
		List<Project> projects = new ArrayList<Project>();
		projects = this.projectService.listProjects();
		for (Project project : projects) {
			if (project.getId() == projectId) {
				int mainGuideProject = project.getMainGuide();
				// gửi thông báo đến các thành viên liên quan có trong project
				List<ProfileProject> profileProjects = this.projectService.getProfileProjects();
				if (profileProjects.size() > 0) {
					for (ProfileProject profileProject : profileProjects) {
						// add thông báo tới các thành viên khác trừ người thực hiện xóa
						if (profileProject.getProjectId() == projectId
								&& profileProject.getProfileId() != mainGuideProject) {
							Notification notification = new Notification();
							notification.setId(0);
							notification.setNoticeFrom(mainGuideProject);
							notification.setNoticeTo(profileProject.getProfileId());
							notification.setContent("removed project " + project.getName());
							notification.setProjectId(projectId);
							notification.setCreatedAt(Timestamp.valueOf(Utils.getCurrentTimeStamp()));
							notification.setFlagRead(0);
							this.projectService.addNotification(notification);
						}
					}
				}
				this.projectService.removeProject(projectId);

			}
		}
		return "OK";
	}

	//////////////// PROJECT - TASKS

	// get tasks belong to specific project
	@RequestMapping(value = "/project/tasks/{project_id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Task> getTasksByProjectId(@PathVariable("project_id") int id) {

		List<Task> taskList = new ArrayList<Task>();
		taskList = this.projectService.listTasks();
		List<Task> tasksOfProject = new ArrayList<Task>();// contain tasks belong to project

		for (Task task : taskList) {
			if (task.getProjectId() == id) {
				tasksOfProject.add(task);
			}
		}
		return tasksOfProject;
	}

	// get tasks belong to profile
	@RequestMapping(value = "/profile/tasks/{profile_id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Task> getTasksBelongProfileId(@PathVariable("profile_id") int id) {

		List<Task> taskList = new ArrayList<Task>();
		taskList = this.projectService.listTasks();
		List<Task> tasksBelongProfile = new ArrayList<Task>();// contain tasks belong to project

		for (Task task : taskList) {
			if (task.getAssignee() == id || task.getCreatedBy() == id) {
				tasksBelongProfile.add(task);
			}
		}
		return tasksBelongProfile;
	}

	// create task
	@RequestMapping(value = "/project/task/add-or-update", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdateTask(@RequestBody Task taskModel) {
		String messageResponse = "";
		List<Task> tasks = this.projectService.listTasks();
		if (taskModel.getId() == 0) {
			this.projectService.addTask(taskModel);
			messageResponse = "Created";
		} else {
			for (Task task : tasks) {
				if (task.getId() == taskModel.getId()) {
					this.projectService.updateTask(taskModel);
					messageResponse = "Updated";
				}
			}
		}

		return messageResponse;
	}
	
	// update status task
	@RequestMapping(value = "/project/task/{task_id}/{status}", method = RequestMethod.POST)
	public @ResponseBody String updateTaskStatus(@PathVariable("task_id") int taskId,@PathVariable("status") String status) {
		String messageResponse = "";
		List<Task> tasks = this.projectService.listTasks();
		if (tasks != null && tasks.size() > 0) {
			for (Task task : tasks) {
				if (task.getId() == taskId) {
					task.setStatus(status);
					this.projectService.updateTask(task);
					messageResponse = "Updated";
					break;
				}
			}
		}
		return messageResponse;
	}

	// remove task
	@RequestMapping(value = "/project/task/remove/{task_id}", method = RequestMethod.POST)
	public @ResponseBody String removeTask(@PathVariable("task_id") int id) {
		List<Task> tasks = this.projectService.listTasks();
		if (tasks.size() > 0) {
			for (Task task : tasks) {
				if (task.getId() == id) {
					this.projectService.removeTask(id);
				}
			}
		} else {
			return "tasks empty";
		}

		return "Removed";
	}

	/////////////////// PROFILE-MEETINGS
	// get all meetings
	@RequestMapping(value = "/meetings", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Meeting> getAllMeetings() {
		return this.projectService.listMeetings();
	}
	
	// get project's meetings
	@RequestMapping(value = "project/meetings/{project_id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Meeting> getProjectMeetings(@PathVariable("project_id") int projectId) {
		List<Meeting> meetings = new ArrayList<Meeting>();
		meetings = this.projectService.listMeetings();
		List<Meeting> projectMeetings = new ArrayList<Meeting>();
		if(meetings!=null && meetings.size()>0) {
			for (Meeting meeting : meetings) {
				if (meeting.getProjectId() == projectId) {
					projectMeetings.add(meeting);
				}
			}

		}
		return projectMeetings;
	}
	
	// get meetings belong to specific profile
	@RequestMapping(value = "/profile/meetings/{profile_id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Meeting> getMeetingByProfileId(@PathVariable("profile_id") int id) {

		List<Meeting> meetings = new ArrayList<Meeting>();
		meetings = this.projectService.listMeetings();
		List<Meeting> meetingsBelongToProfile = new ArrayList<Meeting>();// contain tasks belong to project
		List<ProjectCustom> projectCustoms = getProjectByProfileId(id);

		for (ProjectCustom projectCustom : projectCustoms) {
			for (Meeting meeting : meetings) {
				if (meeting.getProjectId() == projectCustom.getId()) {
					meetingsBelongToProfile.add(meeting);
				}
			}

		}
		return meetingsBelongToProfile;
	}

//	// create meeting
//	@RequestMapping(value = "/project/meeting/add-or-update", method = RequestMethod.POST)
//	public @ResponseBody String addOrUpdateMeeting(@RequestBody Meeting meetingModel) {
//		String messageResponse = "";
//		List<Meeting> meetings = this.projectService.listMeetings();
//		if (meetingModel.getId() == 0) {
//			this.projectService.addMeeting(meetingModel);	
//			messageResponse = "Created";
//		} else {
//			for (Meeting meeting : meetings) {
//				if (meeting.getId() == meetingModel.getId()) {
//					this.projectService.updateMeeting(meetingModel);
//					messageResponse = "Updated";
//				}
//			}
//		}
//
//		return messageResponse;
//
//	}
	
	// create meeting
	@RequestMapping(value = "/project/meeting/add-or-update", method = RequestMethod.POST)
	public @ResponseBody String addOrUpdateMeeting(@RequestBody Meeting meetingModelRequest) {
		String messageResponse = "";
		List<Meeting> meetings = this.projectService.listMeetings();
		if (meetingModelRequest.getId() == 0) {
			this.projectService.addMeeting(meetingModelRequest);
			// tạo cuộc hẹn cho tất cả các thành viên trong project
			// lấy project id
			int projectId = meetingModelRequest.getProjectId();
			// tìm meeting id vừa mới tạo
			int meetingIdJustAdd = 0;
			for (Meeting meeting : this.projectService.listMeetings()) {
				if (meeting.getName().trim().equals(meetingModelRequest.getName().trim())
						&& meeting.getProjectId() == meetingModelRequest.getProjectId()
						&& meeting.getCreatedBy() == meetingModelRequest.getCreatedBy()) {
					meetingIdJustAdd = meeting.getId();// lấy cái add gần nhất có các thông số giống
				}
			}
			// lọc lấy tất cả các thành viên tham gia project này
			for (ProfileProject profileProject : this.projectService.getProfileProjects()) {
				if (profileProject.getProjectId() == projectId) {
					ProfileMeeting profileMeeting = new ProfileMeeting();
					profileMeeting.setMeetingId(meetingIdJustAdd);
					profileMeeting.setProfileId(profileProject.getProfileId());
					if(profileProject.getProfileId()== meetingModelRequest.getCreatedBy()) {
						//người tạo thì set đã đọc và tham gia
						profileMeeting.setFlagRead(1);
						profileMeeting.setFlagJoin(1);
					}else {
						profileMeeting.setFlagRead(0);
						profileMeeting.setFlagJoin(0);
					}

					this.projectService.addProfileMeeting(profileMeeting);
				}
			}

			messageResponse = "Created";
		} else {//trường hợp update meeting
			for (Meeting meeting : meetings) {
				if (meeting.getId() == meetingModelRequest.getId()) {
					this.projectService.updateMeeting(meetingModelRequest);
					//thực hiện update lại trạng thái đọc các meeting khi có sự thay đổi trừ người thực hiện update
					List<ProfileMeeting> profileMeetings = this.projectService.listProfileMeetings();
					if(profileMeetings!=null && profileMeetings.size()>0) {
						for(ProfileMeeting profileMeeting : profileMeetings) {
							if(profileMeeting.getMeetingId()==meetingModelRequest.getId() && profileMeeting.getProfileId()!=meetingModelRequest.getCreatedBy()) {
								profileMeeting.setFlagRead(0);
								this.projectService.updateProfileMeeting(profileMeeting);
							}
						}
					}	
					
					messageResponse = "Updated";
					break;
				}
			}
		}

		return messageResponse;

	}

	// remove meeting
	@RequestMapping(value = "/project/meeting/remove/{meeting_id}", method = RequestMethod.POST)
	public @ResponseBody String removeMeeting(@PathVariable("meeting_id") int id) {
		List<Meeting> meetings = this.projectService.listMeetings();
		if (meetings.size() > 0) {
			for (Meeting meeting : meetings) {
				if (meeting.getId() == id) {
					this.projectService.removeMeeting(id);
				}
			}
		} else {
			return "meeting empty";
		}

		return "Removed";
	}
	//get all profilesMeetings 
	@RequestMapping(value = "/profile-meetings", method = RequestMethod.GET)
	public @ResponseBody List<ProfileMeeting> getProfileMeetings() {
		return this.projectService.listProfileMeetings();
	}
	
	//get profilesMeetings by profile Id
	@RequestMapping(value = "/profile-meetings/{profile_id}", method = RequestMethod.GET)
	public @ResponseBody List<ProfileMeeting> getProfileMeetingsByProfileId(@PathVariable("profile_id") int profileId) {
		List<ProfileMeeting> profileMeetings = this.projectService.listProfileMeetings();
		List<ProfileMeeting> listProfileMeetings = this.projectService.listProfileMeetings();
		if(profileMeetings!=null && profileMeetings.size()>0) {
			for(ProfileMeeting profileMeeting : profileMeetings) {
				if(profileMeeting.getProfileId()==profileId) {
					listProfileMeetings.add(profileMeeting);
				}
			}
		}
		return listProfileMeetings;
	}
	
	
	//update profilesMeetings
	@RequestMapping(value = "meeting/profile-meetings/update", method = RequestMethod.POST)
	public @ResponseBody String updateProfileMeetings(@RequestBody List<ProfileMeeting> profileMeetingsRequest) {
		if (profileMeetingsRequest != null && profileMeetingsRequest.size() > 0) {
			for (ProfileMeeting profileMeetingRequest : profileMeetingsRequest) {
				this.projectService.updateProfileMeeting(profileMeetingRequest);
			}
			updateParticipantsMeeting();
			return "Updated";
		} else {
			return "Requested Null";
		}
	}
	
	// update profilesMeeting
	@RequestMapping(value = "meeting/profile-meeting/update", method = RequestMethod.POST)
	public @ResponseBody String updateProfileMeeting(@RequestBody ProfileMeeting profileMeetingRequest) {
		if (profileMeetingRequest != null) {
			this.projectService.updateProfileMeeting(profileMeetingRequest);
			updateParticipantsMeeting();
			return "Updated";
		} else {
			return "Requested Null";
		}
	}
	
	// update participants meeting
	public void updateParticipantsMeeting() {
		List<ProfileMeeting> profileMeetings = this.projectService.listProfileMeetings();
		List<Meeting> meetings = this.projectService.listMeetings();
		if (meetings != null && meetings.size() > 0) {
			
			for (Meeting meeting : meetings) {
				if (profileMeetings != null && profileMeetings.size() > 0) {
					// đếm số record của profileMeeting có meetingId = meetingId của record meeting
					// để xác định số thành viên có thể tham gia
					int participants = 0;
					for (ProfileMeeting profileMeeting : profileMeetings) {
						if (profileMeeting.getMeetingId() == meeting.getId() && profileMeeting.getFlagJoin() == 1) {
							participants++;
						}
					}
					meeting.setParticipants(participants);
				}
				this.projectService.updateMeeting(meeting);
			}
		}
	}

	/////////////////// PROJECT-NOTIFICATIONS
	// get notification for me
	@RequestMapping(value = "/notifications/{profile_id}", method = RequestMethod.GET)
	public @ResponseBody List<Notification> getNotificationsForProfile(@PathVariable("profile_id") int profileId) {
		List<Notification> profileNotifications = new ArrayList<Notification>();
		List<Notification> notifications = this.projectService.listNotifications();
		if (notifications != null && notifications.size() > 0) {
			for (Notification notification : notifications) {
				if (notification.getNoticeTo() == profileId) {
					profileNotifications.add(notification);
				}
			}
		}

		return profileNotifications;
	}

	// update notification
	@RequestMapping(value = "/notifications/update", method = RequestMethod.POST)
	public @ResponseBody String updateNotification(@RequestBody List<Notification> notificationsRequest) {
		if (notificationsRequest != null && notificationsRequest.size() > 0) {
			for (Notification notificationRequest : notificationsRequest) {
				this.projectService.updateNotification(notificationRequest);
			}
			return "Updated";
		} else {
			return "Requested Null";
		}
	}

}
