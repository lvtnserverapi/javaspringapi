package com.schoolwork.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.schoolwork.spring.model.Contact;
import com.schoolwork.spring.model.Profile;
import com.schoolwork.spring.service.ContactService;

@Controller
public class ContactController {
	private ContactService contactService;

	@Autowired(required = true)
	@Qualifier(value = "contactService")
	public void setContactService(ContactService contactService) {
		this.contactService = contactService;

	}

	@RequestMapping(value = "/contacts", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Contact> listContacts() {

		List<Contact> contactList = new ArrayList<Contact>();
		contactList = this.contactService.listContacts();

		return contactList;

	}

	// get friends of specific profile id
	@RequestMapping(value = {
			"/contacts/{profile_id}" }, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody List<Profile> getProjectById(@PathVariable("profile_id") int id) {
		List<Contact> contacts = new ArrayList<Contact>();
		List<Profile> friends = new ArrayList<Profile>();
		List<Profile> profiles = new ArrayList<Profile>();
		contacts = this.contactService.listContacts();
		profiles = this.contactService.listProfiles();
		for (Contact contact : contacts) {
			if (contact.getProfileId() == id) {
				for (Profile profile : profiles) {
					if (profile.getEmail().equals(contact.getFriendEmail())) {
						friends.add(profile);
						break;
					}
				}
			}
		}
		return friends;
	}

	//Add contact
	@RequestMapping(value = "/contact/add/{email}/{profile_id}", method = RequestMethod.POST)
	public @ResponseBody String addContact(@PathVariable("profile_id") int id, @PathVariable("email") String email) {
		String messageRespone = null;
		List<Profile> profiles = this.contactService.listProfiles();
		List<Contact> contacts = this.contactService.listContacts();
		
		for(Profile profile : profiles) {//check add yourself
			if(profile.getId()==id && profile.getEmail().equals(email)) {
				return "Add yourself";
			}
		}
		
		// check existed friend
		for (Contact contact : contacts) {
			if (contact.getProfileId() == id && contact.getFriendEmail().equals(email)) {
				return messageRespone = "Existed in contact";
			}
		}
		boolean isProfileExist = false;
		for(Profile profile : profiles) {
			if (profile.getEmail().equals(email)) {
				isProfileExist = true;
				break;
			}
		}
		if (!isProfileExist) {
			return "Not exist";
		}

		Contact contact1 = new Contact();
		for (Profile profile : profiles) {
			if (profile.getEmail().equals(email) && profile.getId() != id) {// check to skip me
				contact1.setProfileId(id);
				contact1.setFriendEmail(email);
//				contact1.setFriendName(profile.getFullName());
				this.contactService.addContact(contact1);
				messageRespone = "Added";
			}

		}
		Contact contact2 = new Contact();
		for(Profile profile : profiles) {
			if (profile.getEmail().equals(email) && profile.getId()!=id) {// filter id of friend
				contact2.setProfileId(profile.getId());
			}
			if (profile.getId() == id && !profile.getEmail().equals(email)) {
				contact2.setFriendEmail(profile.getEmail());
//				contact2.setFriendName(profile.getFullName());
			}
		}
		if (contact2 != null) {
			this.contactService.addContact(contact2);
			messageRespone = "Added too";
		}

		return messageRespone;

	}
//remove contact
	@RequestMapping(value = "/contact/remove/{email}/{profile_id}", method = RequestMethod.POST)
	public @ResponseBody String removeContact(@PathVariable("profile_id") int id, @PathVariable("email") String email) {
		String messageRespone = "";
		List<Contact> contacts = this.contactService.listContacts();
		List<Profile> profiles = this.contactService.listProfiles();

		int idFriend = 0;
		for (Profile profile : profiles) {
			if (profile.getEmail().equals(email)) {
				idFriend = profile.getId();
			}
		}
		for (Contact contact : contacts) {
			if (contact.getProfileId() == id && contact.getFriendEmail().equals(email)) {// remove friend
				this.contactService.removeContact(contact.getId());
				messageRespone = "Removed";
			}
			if (contact.getProfileId() == idFriend) {// remove me on friend
				this.contactService.removeContact(contact.getId());
				messageRespone = messageRespone + " both";
			}
		}

		return messageRespone;
	}
	
	//Update contact
	@RequestMapping(value = "/contacts/update", method = RequestMethod.POST ,produces = "application/json;charset=UTF-8")
	public @ResponseBody String updateContact(@RequestBody Contact contactRequest) {
		String messageRespone = null;
		List<Contact> contacts = this.contactService.listContacts();
		for(Contact contact : contacts) {
			if(contact.getId()==contactRequest.getId()) {
				contact.setFriendName(contactRequest.getFriendName());
				messageRespone = "Updated";
			}
		}
		
		return messageRespone;
	}
	
}
