package com.schoolwork.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.schoolwork.spring.dao.CommentDAO;
import com.schoolwork.spring.model.Comment;

@Service
public class CommentServiceImpl implements CommentService {
	
	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	@Override
	@Transactional
	public void addComment(Comment p) {
		this.commentDAO.addComment(p);
	}

	@Override
	@Transactional
	public void updateComment(Comment p) {
		this.commentDAO.updateComment(p);
	}

	@Override
	@Transactional
	public List<Comment> listComments() {
		return this.commentDAO.listComments();
	}

	@Override
	@Transactional
	public Comment getCommentById(int id) {
		return this.commentDAO.getCommentById(id);
	}

	@Override
	@Transactional
	public void removeComment(int id) {
		this.commentDAO.removeComment(id);
	}

}
