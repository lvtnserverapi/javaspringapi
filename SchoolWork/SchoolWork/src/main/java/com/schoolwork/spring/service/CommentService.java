package com.schoolwork.spring.service;

import java.util.List;

import com.schoolwork.spring.model.Comment;

public interface CommentService {
	
	public void addComment(Comment comment);
	public void updateComment(Comment comment);
	public List<Comment> listComments();
	public Comment getCommentById(int id);
	public void removeComment(int id);

}
