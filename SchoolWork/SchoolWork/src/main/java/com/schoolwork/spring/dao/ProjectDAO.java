package com.schoolwork.spring.dao;

import java.util.List;

import com.schoolwork.spring.model.Meeting;
import com.schoolwork.spring.model.Notification;
import com.schoolwork.spring.model.ProfileMeeting;
import com.schoolwork.spring.model.ProfileProject;
import com.schoolwork.spring.model.Project;
import com.schoolwork.spring.model.Task;

public interface ProjectDAO {

	//projects
	public void addProject(Project project);
	public void updateProject(Project project);
	public List<Project> listProjects();
	public Project getProjectById(int id);
	public void removeProject(int id);
	
	//projects - profiles
	public List<ProfileProject> getProfileProjects();
	public void removeProfileProject(int id);
	public void addProfileProject(ProfileProject profileProject);
	
	//project - tasks
	public void addTask(Task task);
	public void updateTask(Task task);
	public List<Task> listTasks();
	public Task getTaskById(int id);
	public void removeTask(int id);
	
	//project - meetings
	public void addMeeting(Meeting meeting);
	public void updateMeeting(Meeting meeting);
	public List<Meeting> listMeetings();
	public Meeting getMeetingById(int id);
	public void removeMeeting(int id);
	//PROFILESMEETINGS
	public void addProfileMeeting(ProfileMeeting profileMeeting);
	public void updateProfileMeeting(ProfileMeeting profileMeeting);
	public List<ProfileMeeting> listProfileMeetings();
	public void removeProfileMeeting(int id);
	
	//project - notifications
	public void addNotification(Notification notification);
	public void updateNotification(Notification notification);
	public List<Notification> listNotifications();
	public void removeNotification(int id);
}
